import 'package:example_clean_architecture/domain/entities/university.dart';

class UniversityTestBuilder {
  String _name = "_name";
  String _country = "_country";
  String _alphaTwoCode = "_alphaTwoCode";

  UniversityTestBuilder withName(String name) {
    name = _name;
    return this;
  }

  University build() {
    return University(_name, _country, _alphaTwoCode);
  }
}

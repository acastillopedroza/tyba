import 'package:bloc_test/bloc_test.dart';
import 'package:example_clean_architecture/domain/entities/university.dart';
import 'package:example_clean_architecture/domain/use_cases/fetch_universities.dart';
import 'package:example_clean_architecture/presentation/bloc/universities_bloc.dart';
import 'package:example_clean_architecture/shared/constanst.dart';
import 'package:mocktail/mocktail.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';

import '../university_test_factory.dart';

class MockFetchUniversities extends Mock implements FetchUniversities {}

void main() {
  group('Universities BLoC', () {
    late FetchUniversities mockFetchUniversities;

    setUp(() {
      mockFetchUniversities = MockFetchUniversities();
    });

    UniversitiesBloc buildBloc() {
      return UniversitiesBloc(mockFetchUniversities);
    }

    group('constructor', () {
      test('works properly', () {
        expect(buildBloc, returnsNormally);
      });

      test('has correct initial state', () {
        expect(buildBloc().state, equals(UniversitiesInitial()));
      });
    });

    group('when Universities fetched is added', () {
      late List<University> universityList;
      late List<University> universityEmptyList;
      setUp(() {
        universityList = UniversityTestFactory.createList();
        universityEmptyList = UniversityTestFactory.createEmptyList();
      });
      blocTest<UniversitiesBloc, UniversitiesState>(
        'with success response then emits state load in progress and load success',
        setUp: (() {
          when(() => mockFetchUniversities()).thenAnswer(
              (realInvocation) => Future.value(Right(universityEmptyList)));
        }),
        build: () => buildBloc(),
        act: (bloc) => bloc.add(UniversitiesFetched()),
        expect: () => [
          isA<UniversitiesLoading>(),
          isA<UniversitiesSuccess>(),
        ],
      );

      blocTest<UniversitiesBloc, UniversitiesState>(
        'with success response then emits state load success with Universities list',
        setUp: (() {
          when(() => mockFetchUniversities()).thenAnswer(
              (realInvocation) => Future.value(Right(universityList)));
        }),
        build: () => buildBloc(),
        act: (bloc) => bloc.add(UniversitiesFetched()),
        expect: () => [
          isA<UniversitiesLoading>(),
          UniversitiesSuccess(universityList),
        ],
      );

      blocTest<UniversitiesBloc, UniversitiesState>(
        'with failure response then emits state load failure',
        setUp: (() {
          when(() => mockFetchUniversities())
              .thenAnswer((realInvocation) async => const Left(unknownError));
        }),
        build: () => buildBloc(),
        act: (bloc) => bloc.add(UniversitiesFetched()),
        expect: () => [
          isA<UniversitiesLoading>(),
          isA<UniversitiesFailure>(),
        ],
      );
    });
  });
}

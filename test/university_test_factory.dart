import 'package:example_clean_architecture/domain/entities/university.dart';

import 'university_test_builder.dart';

class UniversityTestFactory {
  static List<University> createEmptyList() => [];

  static List<University> createList({int length = 2}) {
    return List.generate(
      length,
      (index) => UniversityTestBuilder().build(),
    );
  }
}

import 'package:example_clean_architecture/domain/repositories/university_repository.dart';
import 'package:example_clean_architecture/domain/use_cases/fetch_universities.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:flutter_test/flutter_test.dart';

class MockUniversityRepository extends Mock implements UniversityRepository {}

main() {
  group('Fetch universities', () {
    late UniversityRepository mockUniversityRepository;

    setUp(() {
      mockUniversityRepository = MockUniversityRepository();
    });

    test('when fetch then is success', () async {
      // Arrange
      when(mockUniversityRepository.fetch).thenAnswer((_) async => []);
      final fetchUniversities = FetchUniversities(mockUniversityRepository);

      // Action
      final result = await fetchUniversities();

      // Assert
      expect(result.isRight(), isTrue);
    });

    test('when fetch then is failure', () async {
      // Arrange
      when(mockUniversityRepository.fetch)
          .thenAnswer((_) async => throw Exception());
      final fetchUniversities = FetchUniversities(mockUniversityRepository);

      // Action
      final result = await fetchUniversities();

      // Assert
      expect(result.isLeft(), isTrue);
    });
  });
}

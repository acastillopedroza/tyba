import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'view_mode_state.dart';

enum ViewMode { list, grid }

class ViewModeCubit extends Cubit<ViewModeState> {
  ViewModeCubit() : super(ViewModeListView());

  changeViewMode(ViewMode viewMode) {
    switch (viewMode) {
      case ViewMode.list:
        emit(ViewModeListView());
        break;

      case ViewMode.grid:
        emit(ViewModeGridView());
        break;

      default:
        emit(ViewModeListView());
    }
  }
}

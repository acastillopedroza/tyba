part of 'view_mode_cubit.dart';

@immutable
abstract class ViewModeState {}

class ViewModeListView extends ViewModeState {}

class ViewModeGridView extends ViewModeState {}

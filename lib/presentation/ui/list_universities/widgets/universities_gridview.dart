import 'package:example_clean_architecture/domain/entities/university.dart';
import 'package:example_clean_architecture/presentation/ui/list_universities/widgets/item_university_gridview.dart';
import 'package:flutter/material.dart';

class UniversitiesGridView extends StatelessWidget {
  const UniversitiesGridView({Key? key, required this.universities})
      : super(key: key);

  final List<University> universities;

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: universities
          .map((university) => ItemGridView(university: university))
          .toList(),
    );
  }
}

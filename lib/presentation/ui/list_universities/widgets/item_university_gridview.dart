import 'package:example_clean_architecture/domain/entities/university.dart';
import 'package:example_clean_architecture/presentation/ui/detail_university/detail_university_screen.dart';
import 'package:flutter/material.dart';

class ItemGridView extends StatelessWidget {
  const ItemGridView({Key? key, required this.university}) : super(key: key);

  final University university;

  void _navigation(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailUniversityScreen(university: university),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _navigation(context),
      child: Container(
        height: 128,
        margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16.0),
          boxShadow: [BoxShadow(color: Colors.grey[400]!, blurRadius: 4)],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const CircleAvatar(
              radius: 42,
              backgroundColor: Colors.green,
              child: Icon(
                Icons.home_filled,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 8.0),
            Text(
              university.name,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 4.0),
            Text(university.country),
          ],
        ),
      ),
    );
  }
}

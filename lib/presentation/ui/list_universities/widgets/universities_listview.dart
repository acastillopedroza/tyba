import 'package:example_clean_architecture/domain/entities/university.dart';
import 'package:example_clean_architecture/presentation/ui/list_universities/widgets/item_university_listview.dart';
import 'package:flutter/material.dart';

class UniversitiesListView extends StatelessWidget {
  const UniversitiesListView({Key? key, required this.universities})
      : super(key: key);

  final List<University> universities;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: universities.length,
      itemBuilder: (BuildContext context, int index) {
        return ItemListView(university: universities[index]);
      },
    );
  }
}

import 'package:example_clean_architecture/presentation/cubit/view_mode_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class IconViewMode extends StatelessWidget {
  const IconViewMode({
    Key? key,
    required this.icon,
    required this.viewMode,
  }) : super(key: key);

  final IconData icon;
  final ViewMode viewMode;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final cubit = BlocProvider.of<ViewModeCubit>(context);
        cubit.changeViewMode(viewMode);
      },
      child: Icon(icon, size: 32),
    );
  }
}

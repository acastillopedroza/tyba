import 'package:example_clean_architecture/di/di_container.dart';
import 'package:example_clean_architecture/presentation/bloc/universities_bloc.dart';
import 'package:example_clean_architecture/presentation/cubit/view_mode_cubit.dart';
import 'package:example_clean_architecture/presentation/ui/list_universities/universities_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UniversitiesScreen extends StatelessWidget {
  const UniversitiesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              serviceLocator<UniversitiesBloc>()..add(UniversitiesFetched()),
        ),
        BlocProvider(
          create: (context) => ViewModeCubit(),
        ),
      ],
      child: const UniversitiesView(),
    );
  }
}

import 'package:example_clean_architecture/domain/entities/university.dart';
import 'package:example_clean_architecture/presentation/bloc/universities_bloc.dart';
import 'package:example_clean_architecture/presentation/cubit/view_mode_cubit.dart';
import 'package:example_clean_architecture/presentation/ui/list_universities/widgets/universities_gridview.dart';
import 'package:example_clean_architecture/presentation/ui/list_universities/widgets/universities_listview.dart';
import 'package:example_clean_architecture/presentation/ui/list_universities/widgets/icon_view_mode.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UniversitiesView extends StatelessWidget {
  const UniversitiesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            header(context),
            const SizedBox(height: 16.0),
            Expanded(
              child: BlocBuilder<UniversitiesBloc, UniversitiesState>(
                builder: (BuildContext context, state) {
                  if (state is UniversitiesSuccess) {
                    return viewContent(state.universities);
                  }
                  if (state is UniversitiesFailure) {
                    return viewFailure();
                  }
                  return const Center(child: CircularProgressIndicator());
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget header(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "List of universities",
            style: Theme.of(context).textTheme.titleLarge,
          ),
          Row(
            children: const [
              IconViewMode(icon: Icons.list, viewMode: ViewMode.list),
              SizedBox(width: 4),
              IconViewMode(icon: Icons.grid_3x3, viewMode: ViewMode.grid),
            ],
          ),
        ],
      ),
    );
  }

  Widget viewContent(List<University> universities) {
    return BlocBuilder<ViewModeCubit, ViewModeState>(
      builder: (BuildContext context, stateModeView) {
        if (stateModeView is ViewModeListView) {
          return UniversitiesListView(universities: universities);
        }
        return UniversitiesGridView(universities: universities);
      },
    );
  }

  Widget viewFailure() {
    return const Text("Ocurrio un errror");
  }
}

import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class LoadImage extends StatefulWidget {
  const LoadImage({Key? key}) : super(key: key);

  @override
  _LoadImageState createState() => _LoadImageState();
}

class _LoadImageState extends State<LoadImage> {
  final imagePicker = ImagePicker();
  Uint8List? filePhoto;

  Future<void> _openImagePicker(ImageSource imageSource) async {
    final imagePicker = ImagePicker();
    final file = await imagePicker.pickImage(source: imageSource);
    if (file != null) {
      final bynario = await file.readAsBytes();
      setState(() {
        filePhoto = bynario;
      });
    }
  }

  void _openCamera() async {
    await _openImagePicker(ImageSource.camera);
  }

  void _openGallery() async {
    await _openImagePicker(ImageSource.gallery);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        imageUniversity(),
        const SizedBox(height: 16.0),
        buttonAttachPhoto(),
      ],
    );
  }

  Widget imageUniversity() {
    return filePhoto != null
        ? AspectRatio(
            aspectRatio: 16 / 9,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16.0),
                image: DecorationImage(
                  image: MemoryImage(filePhoto!),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          )
        : Container();
  }

  Widget buttonAttachPhoto() {
    return ElevatedButton(
      onPressed: () {
        showBottomSheet(
          context: context,
          builder: (_) {
            return bottomSheet();
          },
        );
      },
      child: const Text("Adjuntar foto"),
    );
  }

  Widget bottomSheet() {
    return Container(
      height: 128,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.0),
        boxShadow: [BoxShadow(color: Colors.grey[300]!, blurRadius: 4)],
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          itemButtonSheet(
            icon: Icons.camera_alt_outlined,
            title: "Camara",
            onTap: _openCamera,
          ),
          itemButtonSheet(
            icon: Icons.photo,
            title: "Galeria",
            onTap: _openGallery,
          ),
        ],
      ),
    );
  }

  Widget itemButtonSheet({
    required IconData icon,
    required String title,
    required Function() onTap,
  }) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        onTap();
      },
      child: Column(
        children: [
          Icon(icon, size: 64),
          Text(title),
        ],
      ),
    );
  }
}

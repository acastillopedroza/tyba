import 'package:flutter/material.dart';

class ButtonBack extends StatelessWidget {
  const ButtonBack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Row(
        children: const [Icon(Icons.arrow_back_ios), Text("Atrás")],
      ),
    );
  }
}

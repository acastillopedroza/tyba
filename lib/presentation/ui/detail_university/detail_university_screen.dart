import 'package:example_clean_architecture/domain/entities/university.dart';
import 'package:example_clean_architecture/presentation/ui/detail_university/widgets/button_back.dart';
import 'package:example_clean_architecture/presentation/ui/detail_university/widgets/load_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DetailUniversityScreen extends StatelessWidget {
  const DetailUniversityScreen({Key? key, required this.university})
      : super(key: key);

  final University university;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const ButtonBack(),
                const SizedBox(height: 16),
                Text(university.name),
                const SizedBox(height: 8),
                Text(university.country),
                const SizedBox(height: 8),
                Text(university.alphaTwoCode),
                const SizedBox(height: 16),
                const LoadImage(),
                _inputNumberStudents(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _inputNumberStudents() {
    return TextFormField(
      decoration: const InputDecoration(labelText: "Número de estudiantes"),
      inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9]'))],
    );
  }
}

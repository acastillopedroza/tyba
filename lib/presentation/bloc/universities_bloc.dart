import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:example_clean_architecture/domain/entities/university.dart';
import 'package:example_clean_architecture/domain/use_cases/fetch_universities.dart';

part 'universities_event.dart';
part 'universities_state.dart';

class UniversitiesBloc extends Bloc<UniversitiesEvent, UniversitiesState> {
  UniversitiesBloc(this._fetchUniversities) : super(UniversitiesInitial()) {
    on<UniversitiesFetched>(_universitiesFetched);
  }

  final FetchUniversities _fetchUniversities;

  _universitiesFetched(
    UniversitiesFetched event,
    Emitter<UniversitiesState> emit,
  ) async {
    emit(UniversitiesLoading());
    final result = await _fetchUniversities();
    result.fold(
      (String failure) => emit(UniversitiesFailure()),
      (List<University> universities) =>
          emit(UniversitiesSuccess(universities)),
    );
  }
}

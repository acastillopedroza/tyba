part of 'universities_bloc.dart';

abstract class UniversitiesEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class UniversitiesFetched extends UniversitiesEvent {}

part of 'universities_bloc.dart';

abstract class UniversitiesState extends Equatable {
  @override
  List<Object> get props => [];
}

class UniversitiesInitial extends UniversitiesState {}

class UniversitiesLoading extends UniversitiesState {}

class UniversitiesSuccess extends UniversitiesState {
  UniversitiesSuccess(this.universities);

  final List<University> universities;
}

class UniversitiesFailure extends UniversitiesState {}

import 'package:example_clean_architecture/domain/entities/university.dart';

abstract class UniversityRepository {
  Future<List<University>> fetch();
}

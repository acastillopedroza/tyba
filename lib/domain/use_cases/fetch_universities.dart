import 'package:dartz/dartz.dart';
import 'package:example_clean_architecture/domain/entities/university.dart';
import 'package:example_clean_architecture/domain/repositories/university_repository.dart';
import 'package:example_clean_architecture/shared/constanst.dart';

class FetchUniversities {
  FetchUniversities(this._universityRepository);

  final UniversityRepository _universityRepository;

  Future<Either<String, List<University>>> call() async {
    try {
      final universities = await _universityRepository.fetch();
      return Right(universities);
    } catch (_) {
      return const Left(unknownError);
    }
  }
}

class University {
  University(this.name, this.country, this.alphaTwoCode);

  final String name;
  final String country;
  final String alphaTwoCode;
}

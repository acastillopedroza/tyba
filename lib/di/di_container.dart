import 'package:dio/dio.dart';
import 'package:example_clean_architecture/data/data_sources/university_retrofit.dart';
import 'package:example_clean_architecture/data/repositories/university_repository_retrofit.dart';
import 'package:example_clean_architecture/domain/repositories/university_repository.dart';
import 'package:example_clean_architecture/domain/use_cases/fetch_universities.dart';
import 'package:example_clean_architecture/presentation/bloc/universities_bloc.dart';
import 'package:example_clean_architecture/shared/constanst.dart';
import 'package:get_it/get_it.dart';

final serviceLocator = GetIt.instance;

void initDiContainer() {
  serviceLocator.registerFactory(() => UniversitiesBloc(serviceLocator()));

  serviceLocator.registerLazySingleton(
    () => FetchUniversities(serviceLocator()),
  );

  serviceLocator.registerLazySingleton<UniversityRepository>(
    () => UniversityRepositoryRetrofit(serviceLocator()),
  );

  serviceLocator.registerLazySingleton(
    () => UniversityRetrofit(
      serviceLocator(),
      baseUrl: baseUrl,
    ),
  );

  serviceLocator.registerLazySingleton(() => Dio());
}

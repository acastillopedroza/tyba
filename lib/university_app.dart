import 'package:example_clean_architecture/presentation/ui/list_universities/universities_screen.dart';
import 'package:flutter/material.dart';

class UniversityApp extends StatelessWidget {
  const UniversityApp({Key? key}) : super(key: key);

  static const nameApp = 'University App';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: nameApp,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const UniversitiesScreen(),
    );
  }
}

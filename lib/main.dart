import 'package:example_clean_architecture/di/di_container.dart';
import 'package:example_clean_architecture/university_app.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  initDiContainer();
  runApp(const UniversityApp());
}

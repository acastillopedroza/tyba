import 'package:example_clean_architecture/data/data_sources/university_dto.dart';
import 'package:example_clean_architecture/domain/entities/university.dart';

extension UniversityDtoExt on UniversityDto {
  University toUniversityDomain() {
    return University(name, country, alphaTwoCode);
  }
}

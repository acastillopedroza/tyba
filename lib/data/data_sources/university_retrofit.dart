import 'package:dio/dio.dart';
import 'package:example_clean_architecture/data/data_sources/university_dto.dart';
import 'package:example_clean_architecture/shared/constanst.dart';
import 'package:retrofit/retrofit.dart';

part 'university_retrofit.g.dart';

@RestApi()
abstract class UniversityRetrofit {
  factory UniversityRetrofit(Dio dio, {String baseUrl}) = _UniversityRetrofit;

  @GET(apiUniversity)
  Future<HttpResponse<List<UniversityDto>>> fetchUniversities();
}

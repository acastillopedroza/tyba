import 'package:json_annotation/json_annotation.dart';

part 'university_dto.g.dart';

@JsonSerializable()
class UniversityDto {
  UniversityDto(this.name, this.country, this.alphaTwoCode);

  final String name;
  final String country;
  @JsonKey(name: "alpha_two_code")
  final String alphaTwoCode;

  factory UniversityDto.fromJson(Map<String, dynamic> json) =>
      _$UniversityDtoFromJson(json);
  Map<String, dynamic> toJson() => _$UniversityDtoToJson(this);
}

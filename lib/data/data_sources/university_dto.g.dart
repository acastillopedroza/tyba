// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'university_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UniversityDto _$UniversityDtoFromJson(Map<String, dynamic> json) =>
    UniversityDto(
      json['name'] as String,
      json['country'] as String,
      json['alpha_two_code'] as String,
    );

Map<String, dynamic> _$UniversityDtoToJson(UniversityDto instance) =>
    <String, dynamic>{
      'name': instance.name,
      'country': instance.country,
      'alpha_two_code': instance.alphaTwoCode,
    };

import 'dart:io';

import 'package:example_clean_architecture/data/data_sources/university_retrofit.dart';
import 'package:example_clean_architecture/data/data_sources/university_ext.dart';

import 'package:example_clean_architecture/domain/entities/university.dart';
import 'package:example_clean_architecture/domain/repositories/university_repository.dart';

class UniversityRepositoryRetrofit implements UniversityRepository {
  final UniversityRetrofit _universityRetrofit;

  UniversityRepositoryRetrofit(this._universityRetrofit);
  @override
  Future<List<University>> fetch() async {
    final result = await _universityRetrofit.fetchUniversities();
    if (result.response.statusCode != HttpStatus.ok) {
      throw Exception();
    }
    return result.data
        .map((university) => university.toUniversityDomain())
        .toList();
  }
}
